package com.jsonpath;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

public class JsonPathTraversing {

	@Test
	public void displayAllUsersName() {
		Configuration configuration = Configuration.defaultConfiguration();
		try {
			DocumentContext context = JsonPath.using(configuration).parse(new File("./resourses/sample-data.json"));
			@SuppressWarnings("unchecked")
			List<String> names = context.read("$[*].name", List.class);
			System.out.println(names);
			Reporter.log("All users count : " + names.size() + "<br>");
			Reporter.log("All users name: " + names);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Test
	public void displayAllFemaleUsers() {
		try {
			DocumentContext context = JsonPath.parse(new File("./resourses/sample-data.json"),
					Configuration.defaultConfiguration());
			@SuppressWarnings("unchecked")
			List<String> femaleUsers = context.read("$[?(@.gender==\"female\")]", List.class);
			System.out.println(femaleUsers);
			Reporter.log("All female users count : " + femaleUsers.size() + "<br>");
			Reporter.log("All female users : " + femaleUsers);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void displayAllMaleUsers() {
		try {
			DocumentContext context = JsonPath.parse(new File("./resourses/sample-data.json"),
					Configuration.defaultConfiguration());
			@SuppressWarnings("unchecked")
			List<String> maleUsers = context.read("$[?(@.gender==\"male\")]", List.class);
			System.out.println(maleUsers);
			Reporter.log("All male users count : " + maleUsers.size() + "<br>");
			Reporter.log("All male users : " + maleUsers);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void displayFirstTwoUsers() {
		try {
			DocumentContext context = JsonPath.parse(new File("./resourses/sample-data.json"),
					Configuration.defaultConfiguration());
			@SuppressWarnings("unchecked")
			List<String> firstTwoUsers = context.read("$[:2]", List.class);
			System.out.println(firstTwoUsers);
			Reporter.log("Users count : " + firstTwoUsers.size() + "<br>");
			Reporter.log("First two users : " + firstTwoUsers);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void displayLastTwoUsers() {
		try {
			DocumentContext context = JsonPath.parse(new File("./resourses/sample-data.json"),
					Configuration.defaultConfiguration());
			@SuppressWarnings("unchecked")
			List<String> lastTwoUsers = context.read("$[-2:]", List.class);
			System.out.println(lastTwoUsers);
			Reporter.log("Users count : " + lastTwoUsers.size() + "<br>");
			Reporter.log("Last two users : " + lastTwoUsers);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void displayTotalNumberOfFrindsForFirstUser() {
		try {
			DocumentContext context = JsonPath.parse(new File("./resourses/sample-data.json"),
					Configuration.defaultConfiguration());
			@SuppressWarnings("unchecked")
			List<String> firstUserFriends = context.read("$[:1].friends[*].name", List.class);
			System.out.println(firstUserFriends);
			Reporter.log("First user friends count : " + firstUserFriends.size() + "<br>");
			Reporter.log("First user friends : " + firstUserFriends);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void displayAllUsersBeweenAge24to36() {
		try {
			DocumentContext context = JsonPath.parse(new File("./resourses/sample-data.json"),
					Configuration.defaultConfiguration());
			@SuppressWarnings("unchecked")
			List<String> allUserInAgeRange = context.read("$[?(@.age>24 && @.age<36)]", List.class);
			System.out.println(allUserInAgeRange);
			Reporter.log("All Users Beween Age 24 to 36 count : " + allUserInAgeRange.size() + "<br>");
			Reporter.log("All Users Beween Age 24 to 36 : " + allUserInAgeRange);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void displayAllUsersWhoLivesInCalifornia() {
		try {
			DocumentContext context = JsonPath.parse(new File("./resourses/sample-data.json"),
					Configuration.defaultConfiguration());
			@SuppressWarnings("unchecked")
			List<String> allUserInAgeRange = context.read("$[?(@.address=~ /.*California.*/i)]", List.class);
			System.out.println(allUserInAgeRange);
			Reporter.log("All Users Who Lives In California count : " + allUserInAgeRange.size() + "<br>");
			Reporter.log("All Users Who Lives In California  : " + allUserInAgeRange);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
